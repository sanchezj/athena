# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

#####
# CI Reference Files Map
#####

# The top-level directory for the files is /eos/atlas/atlascerngroupdisk/data-art/grid-input/WorkflowReferences/
# Then the subfolders follow the format branch/test/version, i.e. for s3760 in master the reference files are under
# /eos/atlas/atlascerngroupdisk/data-art/grid-input/WorkflowReferences/main/s3760/v1 for v1 version

# Format is "test" : "version"
references_map = {
    # Simulation
    "s3761": "v10",
    "s3779": "v2",
    "s4005": "v6",
    "s4006": "v9",
    "s4007": "v7",
    "s4008": "v1",
    "a913": "v5",
    # Overlay
    "d1726": "v6",
    "d1759": "v12",
    "d1912": "v1",
    # Reco
    "q442": "v43",
    "q449": "v67",
    "q452": "v1",
    "q454": "v1",
    # Derivations
    "data_PHYS_Run2": "v16",
    "data_PHYS_Run3": "v15",
    "mc_PHYS_Run2": "v19",
    "mc_PHYS_Run3": "v19",
}
